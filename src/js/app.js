var app = angular.module('xwingApp', ['ui.bootstrap', 'ngRoute', 'ngStorage', 'pascalprecht.translate', 'ui-notification', 'utf8-base64', 'ngFitText']);

app.config(
    ['$routeProvider', '$translateProvider', 'NotificationProvider', function ($routeProvider, $translateProvider, NotificationProvider) {
        $routeProvider
            .when(
                '/', {
                    templateUrl: 'pages/home.html',
                    controller: 'homeController'
                }
            )
            .when(
                '/list', {
                    templateUrl: 'pages/list.html',
                    controller: 'listController'
                }
            )
            .when(
                '/play', {
                    templateUrl: 'pages/play.html',
                    controller: 'playController'
                }
            )
            .when(
                '/settings', {
                    templateUrl: 'pages/settings.html',
                    controller: 'settingsController'
                }
            )
            .when(
                '/client', {
                    templateUrl: 'pages/client.html',
                    controller: 'clientController'
                }
            );
        $translateProvider.useStaticFilesLoader(
            {
                prefix: 'locale/locale-',
                suffix: '.json'
            }
        );
        $translateProvider.preferredLanguage('en');
        $translateProvider.useSanitizeValueStrategy(null);
        NotificationProvider.setOptions(
            {
                delay: 5000,
                startTop: 20,
                startRight: 10,
                verticalSpacing: 20,
                horizontalSpacing: 20,
                positionX: 'right',
                positionY: 'top'
            }
        );

    }]
);

app.filter(
    'stripQuotes', function () {
        return function (str) {
            return str.replace(/\"/g, "");
        };
    }
);

app.filter(
    'stripNonAlphaNumeric', function () {
        return function (str) {
            return str.replace(/[^a-z0-9]+|\s+/gmi, "").toLowerCase();
        };
    }
);

app.filter(
    'translatePilotShip', ['ShipService', function (ShipService) {
        return function (ship) {
            return ShipService.translatePilotShip(ship);
        };
    }]
);

app.filter(
    'translatePilot', ['PilotService', function (PilotService) {
        return function (pilot) {
            return PilotService.translate(pilot);
        };
    }]
);

app.filter(
    'translateFaction', ['FactionService', function (FactionService) {
        return function (faction) {
            return FactionService.translate(faction)
        };
    }]
);

app.service(
    'PilotService', ['$rootScope', '$http', '$filter', '$translate', 'SettingsService', 'Notification', function ($rootScope, $http, $filter, $translate, SettingsService, Notification) {
        var self = this;

        this.addPilot = function (pilot) {
            $rootScope.pilots.push(pilot);
        };

        this.init = function () {
            $rootScope.pilots = [];
            $rootScope.pilots.length = 0;
            $http(
                {
                    method: 'GET',
                    url: 'locale/locale-en-pilots.json'
                }
            ).then(
                function successCallback(response) {
                    if (angular.isDefined(response.data) && angular.isArray(response.data)) {
                        angular.forEach(
                            response.data, function (value) {
                                value.name = $filter('stripQuotes')(value.name);
                                value.active = false;
                                self.addPilot(value);
                            }
                        );
                    } else {
                        Notification.error($translate.instant('errors.pilot-list'));
                    }
                }, function errorCallback(response) {
                    Notification.error($translate.instant('errors.pilot-list'));
                }
            );
        };

        this.translate = function (pilot) {
            return (angular.isDefined($rootScope.pilotsTranslation[pilot.name]) && angular.isDefined(
                $rootScope.pilotsTranslation[pilot.name].name
            )) ? $rootScope.pilotsTranslation[pilot.name].name : pilot.name;
        };

        $rootScope.$on(
            '$translateChangeSuccess', function () {
                $rootScope.pilotsTranslation = [];
                var lang = SettingsService.get("language");
                if (angular.isDefined(lang) && lang != "en") {
                    $http(
                        {
                            method: 'GET',
                            url: 'locale/locale-' + lang + '-pilots.json'
                        }
                    ).then(
                        function successCallback(response) {
                            if (angular.isDefined(response.data) && angular.isObject(response.data)) {
                                $rootScope.pilotsTranslation = response.data;
                                angular.forEach(
                                    $rootScope.pilotsTranslation, function ($value, key) {
                                        if (angular.isDefined($value.name)) {
                                            $value.name = $filter('stripQuotes')($value.name);
                                            $rootScope.pilotsTranslation[key] = ($value);
                                        }
                                    }
                                );
                            } else {
                                Notification.error($translate.instant('errors.pilot-translation'));
                            }
                        }, function errorCallback(response) {
                            Notification.error($translate.instant('errors.pilot-translation'));
                        }
                    );
                }
            }
        );
    }]
);

app.service(
    'ShipService', ['$filter', '$localStorage', '$rootScope', 'FactionService', 'SettingsService', function ($filter, $localStorage, $rootScope, FactionService, SettingsService) {
        var self = this;
        this.init = function () {
            if (!$localStorage.ships || $localStorage.ships.length < 0) {
                $localStorage.ships = [];
            }
        };

        this.addShip = function (ship) {
            ship.uq = Date.now();
            if(!angular.isDefined(ship.skill) || ship.skill == null){
                ship.skill = 0;
            }
            $localStorage.ships.push(ship);
        };

        this.deleteShip = function (ship) {
            var index = $localStorage.ships.indexOf(ship);
            if (index != -1) {
                $localStorage.ships.splice(index, 1);
            }
        };

        this.isShipActive = function (ship) {
            return !!(angular.isDefined(ship.active) && ship.active);
        };

        this.getActiveShips = function () {
            var active = [];
            angular.forEach(
                $localStorage.ships, function ($value) {
                    if (self.isShipActive($value)) {
                        active.push($value);
                    }
                }
            );
            return active;
        };

        this.deactivateShips = function () {
            angular.forEach(
                $localStorage.ships, function ($value) {
                    $value.active = false
                }
            );
        };

        this.activateShip = function (ship) {
            if ($localStorage.ships.indexOf(ship) != -1) {
                ship.active = true;
                ship.played = false;
            }
        };

        this.getLastActiveShip = function () {
            var actives = self.getActiveShips();
            if (actives.length > 0) {
                return actives[actives.length - 1];
            } else {
                return false;
            }
        };

        this.getFirstActiveShip = function () {
            var actives = self.getActiveShips();
            if (actives.length > 0) {
                return actives[0];
            } else {
                return false;
            }
        };

        this.getSameLevelShips = function (reverse) {
            var actives = self.getActiveShips();
            if (actives.length > 0) {
                var index = $localStorage.ships.indexOf(actives[actives.length - 1]);
                var ship = $localStorage.ships[index];
                var skill = self.getActiveSkill(ship);
                var newIndex;

                if (reverse) {
                    newIndex = index - 1;
                } else {
                    newIndex = index + 1;
                }

                var compare = FactionService.getPrimaryFaction(ship.faction);
                while (angular.isDefined($localStorage.ships[newIndex]) && FactionService.getPrimaryFaction($localStorage.ships[newIndex].faction) == compare && parseInt(
                    $localStorage.ships[newIndex].skill
                ) == skill) {
                    $localStorage.ships[newIndex].active = true;
                    $localStorage.ships[newIndex].played = false;

                    if (reverse) {
                        newIndex--;
                    } else {
                        newIndex++;
                    }
                }
            }
        };

        this.updateShip = function (ship, update) {
            if (!angular.equals(ship, update)) {
                if (angular.isDefined(update.skill2) && angular.isNumber(parseInt(update.skill2))) {
                    ship.skill2 = parseInt(update.skill2);
                } else {
                    delete ship.skill2;
                }
                if (angular.isDefined(update.skill) && update.skill != null) {
                    ship.skill = parseInt(update.skill);
                }else{
                    ship.skill = 0;
                }
                ship.identifier = update.identifier;
            }
        };

        this.getShips = function () {
            return $storage.ships;
        };

        this.sortShipListComparator = function (s1, s2) {
            var phase = SettingsService.get('phase');
            var comp1 = self.getActiveSkill(s1.value);
            var comp2 = self.getActiveSkill(s2.value);

            if (phase === 0) {
                if (comp1 < comp2) {
                    return 1;
                }
            } else {
                if (comp1 > comp2) {
                    return 1;
                }
            }

            if ((comp1 == comp2) && FactionService.isInitiativeFaction(s1.value.faction)) {
                return 1;
            } else {
                return -1;
            }

        };

        this.getActiveSkill = function (ship) {
            return parseInt((angular.isDefined(ship.skill2) && SettingsService.get('phase') === 0) ? ship.skill2 : ship.skill);
        };

        this.sortShips = function (callback) {
            $localStorage.ships = $filter('orderBy')($localStorage.ships, "", true, self.sortShipListComparator);
            if (angular.isFunction(callback)) {
                callback();
            }
        };

        this.translatePilotShip = function (pilot) {
            return (angular.isDefined($rootScope.pilotsTranslation[pilot.name]) && angular.isDefined(
                $rootScope.pilotsTranslation[pilot.name].ship
            )) ? $rootScope.pilotsTranslation[pilot.name].ship : pilot.ship;
        };
    }]
);

app.service(
    'GameService', ['$localStorage', 'SettingsService', 'ShipService', function ($localStorage, SettingsService, ShipService) {
        var self = this;

        this.getPhase = function () {
            return SettingsService.get('phase');
        };

        this.setPhase = function (phase) {
            return SettingsService.set('phase', phase);
        };

        this.getRound = function () {
            return SettingsService.get('round');
        };

        this.setRound = function (round) {
            return SettingsService.set('round', round);
        };

        this.nextShip = function () {
            var lastActive = ShipService.getLastActiveShip();
            if (lastActive) {
                var index = $localStorage.ships.indexOf(lastActive);
                if (angular.isDefined($localStorage.ships[index + 1])) {
                    var newActiveShip = $localStorage.ships[index + 1];
                    ShipService.deactivateShips();
                    ShipService.activateShip(newActiveShip);
                    ShipService.getSameLevelShips();
                } else {
                    self.nextPhase();
                    angular.forEach(
                        ShipService.getActiveShips(), function (value) {
                            value.played = false;
                        }
                    );
                    ShipService.sortShips();
                    ShipService.deactivateShips();
                    ShipService.activateShip($localStorage.ships[0]);
                    ShipService.getSameLevelShips();
                }
            }
        };

        this.previousShip = function () {
            var index = $localStorage.ships.indexOf(ShipService.getFirstActiveShip());
            if (index != -1 && index > 0) {
                ShipService.deactivateShips();
                ShipService.activateShip($localStorage.ships[index - 1]);
                ShipService.getSameLevelShips(true);
            }

        };

        this.restart = function () {
            self.setRound(1);
            self.setPhase(0);
            if ($localStorage.ships.length > 0) {
                ShipService.deactivateShips();
                ShipService.sortShips();
                ShipService.activateShip($localStorage.ships[0]);
                ShipService.getSameLevelShips(false);
            }
        };

        this.nextRound = function () {
            self.checkRoundEffects();
            ShipService.sortShips();
            self.setRound((SettingsService.get('round') + 1));
        };

        this.previousRound = function () {
            var round = SettingsService.get('round');
            if (round > 0) {
                self.setRound((round - 1));
            }

        };

        this.nextPhase = function () {
            if (SettingsService.get('phase') == 0) {
                self.setPhase(1);
            } else {
                self.setPhase(0);
                self.nextRound();
            }
        };

        this.previousPhase = function () {
            self.setPhase((SettingsService.get('phase') == 0 ? 1 : 0));
        };

        this.checkIfAllPlayed = function () {
            var check = true;
            angular.forEach(
                ShipService.getActiveShips(), function (value) {
                    if (angular.isDefined(value.played) && value.played != true) {
                        check = false;
                    }
                }
            );
            if (check) {
                self.nextShip();
            }
        };

        this.checkRoundEffects = function () {
            angular.forEach(
                $localStorage.ships, function (value) {
                    if (value.changes) {
                        var changes = Object.keys(value.changes);
                        angular.forEach(
                            changes, function (acc) {
                                if (acc == "skill2" && value.changes[acc] == null) {
                                    delete value.skill2;
                                } else {
                                    value[acc] = value.changes[acc];
                                }
                            }
                        );
                        delete value.changes;
                    }
                }
            );
        };
    }]
);

app.service(
    'FactionService', ['$translate', 'SettingsService', function ($translate, SettingsService) {
        var self = this;
        this.getPrimaryFaction = function (factionString) {
            switch (factionString) {
                case 'Rebel Alliance':
                case 'Resistance':
                    return 'Rebel Alliance';
                    break;
                case 'Galactic Empire':
                case 'First Order':
                    return 'Galactic Empire';
                    break;
                default:
                    return factionString;
                    break;
            }
        };

        this.getShortFaction = function (factionString) {
            switch (factionString) {
                case 'Rebel Alliance':
                case 'Resistance':
                    return 'rebel';
                    break;
                case 'Galactic Empire':
                case 'First Order':
                    return 'empire';
                    break;
                default:
                    return 'scum';
                    break;
            }
        };

        this.getFactionIconClass = function (factionString) {
            switch (factionString) {
                case 'Galactic Empire':
                    return "xwing-miniatures-font-empire";
                    break;
                case 'First Order':
                    return "xwing-miniatures-font-firstorder";
                    break;
                case 'Rebel Alliance':
                case 'Resistance':
                    return "xwing-miniatures-font-rebel";
                    break;
                case 'Scum and Villainy':
                    return "xwing-miniatures-font-scum";
                    break;
                default:
                    return;
                    break;
            }
        };

        this.isInitiativeFaction = function (faction) {
            return (self.getPrimaryFaction(faction) == SettingsService.get('initiative'));
        };

        this.translate = function (faction) {
            var search = faction.replace(/\s+/g, '');
            return $translate.instant('factions.' + search.toLowerCase());
        };
    }]
);

app.service(
    'SettingsService', ['$localStorage', function ($localStorage) {
        this.init = function () {
            $localStorage.$default(
                {
                    settings: {
                        round: 1,
                        phase: 0,
                        initiative: 'Galactic Empire',
                        progressbar: false,
                        playlist: true,
                        factions: ['Rebel Alliance', 'Galactic Empire', 'Scum and Villainy'],
                        languages: [
                            {name: "English", key: "en"},
                            {name: "Deutsch", key: "de"}
                        ],
                        language: "en"
                    }
                }
            );

            this.set = function (key, value) {
                $localStorage.settings[key] = value;
            };

            this.get = function (key) {
                return $localStorage.settings[key];
            }
        }
    }]
);

app.run(
    ['$translate', '$templateRequest', 'SettingsService', 'ShipService', 'PilotService',
        function ($translate, $templateRequest, SettingsService, ShipService, PilotService) {
            $templateRequest('pages/shipEditModal.html', true);
            SettingsService.init();
            ShipService.init();
            PilotService.init();

            if ($translate.use() != SettingsService.get('language')) {
                $translate.use(SettingsService.get('language'));
                $translate.fallbackLanguage("en");
            }
        }]
);

app.controller(
    "mainController", ['$scope', function ($scope) {
        $scope.navigation = 'home';

        $scope.init = function () {

        };

        $scope.init();

        $scope.isRunningInElectron = function () {
            // TODO simplier solution
            if (typeof process !== "undefined") {
                if (typeof process.versions !== "undefined") {
                    if (typeof process.versions.electron !== "undefined") {
                        return true;

                    }
                }
            }
            return false;
        }
    }]
);

app.controller(
    "homeController", ['$scope', function ($scope) {
        $scope.$parent.navigation = 'home';
    }]
);

app.controller(
    "listController", ['$scope', '$uibModal', '$localStorage', 'ShipService', 'FactionService', 'PilotService', 'GameService', function ($scope, $uibModal, $localStorage, ShipService, FactionService, PilotService, GameService) {
        $scope.$parent.navigation = 'list';
        $scope.checkModel = 'all';
        $scope.orderByField = 'skill';
        $scope.reverseSort = false;
        $scope.$storage = $localStorage;

        $scope.getOptions = function (pilot) {
            return PilotService.translate(pilot) + ' | ' + ShipService.translatePilotShip(pilot) + ' - ' + FactionService.translate(pilot.faction) + ' (' + pilot.skill + ')';
        };

        $scope.orderByTranslatedPilotName = function (pilot) {
            return PilotService.translate(pilot);
        };

        $scope.selection = function (item) {
            if ($scope.checkModel == 'all') {
                return item;
            } else if (FactionService.getShortFaction(item.faction) == $scope.checkModel) {
                return item;
            }
        };

        $scope.destroyShip = function (ship) {
            if (ShipService.isShipActive(ship)) {
                if (ShipService.getActiveShips().length == 1) {
                    GameService.nextShip();
                }
            }
            ShipService.deleteShip(ship);
        };

        $scope.openShipEdit = function (ship) {
            $uibModal.open(
                {
                    templateUrl: 'pages/shipEditModal.html',
                    controller: 'shipListEditModalController',
                    controllerAs: '$ctrl',
                    size: 'lg',
                    resolve: {
                        item: function () {
                            return ship;
                        },
                        translateScope: function () {
                            return $scope.$parent.navigation
                        }
                    }
                }
            );
        };

        $scope.quickAddShip = function () {
            if (angular.isDefined($scope.selectedItem)) {
                var item = angular.copy($scope.selectedItem);
                ShipService.addShip(item);
            }
        };

        $scope.addShip = function () {
            if (angular.isDefined($scope.selectedItem)) {
                var item = angular.copy($scope.selectedItem);
                $scope.openShipEdit(item);
            }
        };
    }]
);

app.controller(
    'shipListEditModalController', ['$uibModalInstance', '$localStorage', '$scope', 'ShipService', 'item', 'translateScope', function ($uibModalInstance, $localStorage, $scope, ShipService, item, translateScope) {
        var $ctrl = this;
        $ctrl.skillAddition = false;
        $ctrl.item = item;
        $ctrl.update = angular.copy(item);
        $ctrl.translateScope = translateScope;

        if (angular.isDefined($ctrl.update.skill2) && angular.isNumber($ctrl.update.skill2)) {
            $ctrl.skillAddition = true;
        }

        $scope.$watch(
            "$ctrl.skillAddition", function () {
                if (!$ctrl.skillAddition) {
                    delete $ctrl.update.skill2;
                }
            }
        );

        $ctrl.ok = function () {
            if ($localStorage.ships.indexOf($ctrl.item) == -1) {
                ShipService.addShip($ctrl.update);
            } else {
                ShipService.updateShip($ctrl.item, $ctrl.update);
            }
            ShipService.sortShips();
            $uibModalInstance.close();
        };

        $ctrl.reset = function () {
            $ctrl.update.skill = $ctrl.item.skill;
            $ctrl.update.identifier = $ctrl.item.identifier;
            if (angular.isDefined($ctrl.item.skill2) && angular.isNumber($ctrl.item.skill2)) {
                $ctrl.update.skill2 = $ctrl.item.skill2;
                $ctrl.skillAddition = true;
            } else {
                $ctrl.skillAddition = false;
                delete $ctrl.update.skill2;
            }
        };
    }]
);

app.controller(
    "playController", ['$scope', '$uibModal', '$localStorage', '$translate', 'GameService', 'FactionService', 'ShipService', 'SettingsService', function ($scope, $uibModal, $localStorage, $translate, GameService, FactionService, ShipService, SettingsService) {
        $scope.$parent.navigation = 'play';
        $scope.gameend = false;
        $scope.won = null;
        $scope.progress = 0;
        $scope.progressStyle = {};
        $scope.$storage = $localStorage;
        $scope.playlist = SettingsService.get('playlist');

        $scope.getShipCardClasses = function (faction, played) {
            var classes = FactionService.getShortFaction(faction);
            if (angular.isDefined(played) && played == true) {
                classes += " played";
            }
            return classes;
        };

        $scope.played = function (ship) {
            ship.played = true;
            GameService.checkIfAllPlayed();
        };

        $scope.init = function () {
            $scope.gameend = false;
            $scope.won = null;
            ShipService.sortShips();

            if ($scope.$storage.ships.length > 0) {
                if (ShipService.getActiveShips().length == 0) {
                    $scope.$storage.ships[0].active = true;
                }
            }

            ShipService.getSameLevelShips(false);
            $scope.$watch(
                "$storage.settings.phase", function () {
                    $scope.phaseHeadline = ($scope.$storage.settings.phase == 0 ? $translate.instant('play.activation') : $translate.instant('play.combat'));
                }
            );
        };

        $scope.back = function () {
            GameService.previousShip();
        };

        $scope.next = function () {
            GameService.nextShip();

        };

        $scope.restart = function () {
            GameService.restart();
        };

        $scope.destroyShip = function (ship) {
            if (ShipService.isShipActive(ship)) {
                if (ShipService.getActiveShips().length == 1) {
                    GameService.nextShip();
                }
            }
            ShipService.deleteShip(ship);

            var activeFactions = [];

            angular.forEach(
                $scope.$storage.ships, function (value) {
                    activeFactions[FactionService.getPrimaryFaction(value.faction)] = true;
                }
            );

            if (Object.keys(activeFactions).length == 1) {
                $scope.gameend = true;
                $scope.won = FactionService.getShortFaction($scope.$storage.ships[0].faction);
                GameService.setRound(1);
                GameService.setPhase(1);
                $scope.$storage.ships.length = 0;
            }
        };

        $scope.getFactionIcon = function (faction) {
            return FactionService.getFactionIconClass(faction);
        };

        $scope.openShipEdit = function (ship) {
            $uibModal.open(
                {
                    templateUrl: 'pages/shipEditModal.html',
                    controller: 'playListEditModalController',
                    controllerAs: '$ctrl',
                    size: 'lg',
                    resolve: {
                        item: function () {
                            return ship;
                        },
                        translateScope: function () {
                            return $scope.$parent.navigation
                        }
                    }
                }
            );
        };

        if (SettingsService.get('progressbar')) {
            $scope.$watch(
                "$storage.ships", function () {
                    if ($scope.$storage.ships.length > 0) {
                        var lastActive = ShipService.getLastActiveShip();
                        var index = $scope.$storage.ships.indexOf(lastActive);
                        $scope.progress = (index + 1) / $scope.$storage.ships.length;
                        $scope.progressStyle.width = Math.round($scope.progress * 100) + "%"
                    }
                }, true
            );
        }

        $scope.init();
    }]
);

app.controller(
    'playListEditModalController', ['$uibModalInstance', 'item', '$scope', 'translateScope', function ($uibModalInstance, item, $scope, translateScope) {
        var $ctrl = this;
        $ctrl.item = item;
        $ctrl.translateScope = translateScope;
        $ctrl.update = angular.copy(item);
        $ctrl.skillAddition = false;

        if (!angular.isDefined($ctrl.item.changes)) {
            $ctrl.update.changes = {};
        }
        if (angular.isDefined($ctrl.update.changes.skill) && angular.isNumber($ctrl.update.changes.skill)) {
            $ctrl.update.skill = $ctrl.update.changes.skill;
        }

        if (angular.isDefined($ctrl.update.changes.skill2) && angular.isNumber($ctrl.update.changes.skill2)) {
            $ctrl.update.skill2 = $ctrl.update.changes.skill2;
            $ctrl.skillAddition = true;
        }


        if (angular.isDefined($ctrl.update.skill2) && $ctrl.update.changes.skill2 != null) {
            $ctrl.skillAddition = true;
        }

        $scope.$watch(
            "$ctrl.skillAddition", function () {
                if (!$ctrl.skillAddition) {
                    delete $ctrl.update.skill2;
                }
            }
        );

        $ctrl.ok = function () {
            if (angular.isNumber(parseInt($ctrl.update.skill))) {
                $ctrl.update.changes.skill = parseInt($ctrl.update.skill);
            } else {
                delete $ctrl.update.changes.skill;
            }

            if (angular.isNumber(parseInt($ctrl.update.skill2))) {
                $ctrl.update.changes.skill2 = parseInt($ctrl.update.skill2);
            } else {
                delete $ctrl.update.changes.skill2;
            }

            $ctrl.item.changes = $ctrl.update.changes;
            $uibModalInstance.close();
        };

        $scope.resetSkillTwo = function () {
            if (angular.isDefined($ctrl.item.changes)) {
                if (angular.isDefined($ctrl.item.changes.skill2) && angular.isNumber(parseInt($ctrl.item.changes.skill2))) {
                    $ctrl.update.skill2 = parseInt($ctrl.item.changes.skill2);
                    $ctrl.skillAddition = true;
                }
            } else if (angular.isDefined($ctrl.item.skill2) && angular.isNumber(parseInt($ctrl.item.skill2))) {
                $ctrl.update.skill2 = parseInt($ctrl.item.skill2);
                $ctrl.skillAddition = true;
            } else {
                $ctrl.skillAddition = false;
                delete $ctrl.update.skill2;
            }
        };

        $scope.resetSkillOne = function () {
            if (angular.isDefined($ctrl.item.changes) && angular.isDefined($ctrl.item.changes.skill) && angular.isNumber($ctrl.item.changes.skill)) {
                $ctrl.update.skill = $ctrl.item.changes.skill;
            } else {
                $ctrl.update.skill = $ctrl.item.skill;
            }
        };

        $ctrl.reset = function () {
            $scope.resetSkillOne();
            $scope.resetSkillTwo();
        };
    }]
);

app.controller(
    "settingsController", ['$scope', '$translate', '$localStorage', 'SettingsService', 'base64', 'Notification', 'GameService', 'FactionService', function ($scope, $translate, $localStorage, SettingsService, base64, Notification, GameService, FactionService) {
        $scope.$parent.navigation = 'settings';
        $scope.languages = SettingsService.get("languages");
        $scope.factions = SettingsService.get("factions");
        $scope.$storage = $localStorage;
        $scope.settingsSet = false;
        $scope.importData = false;

        $scope.changeLanguage = function () {
            SettingsService.set("language", $scope.selectedLanguage.key);
            $translate.use($scope.selectedLanguage.key);
        };

        $scope.changeInitiative = function () {
            SettingsService.set("initiative", $scope.selectedInitiative);
            GameService.restart();
        };

        $scope.getTranslatedFaction = function (faction) {
            return FactionService.translate(faction);
        };

        $scope.init = function () {
            angular.forEach(
                $scope.languages, function (value) {
                    if (value.key == SettingsService.get("language")) {
                        $scope.selectedLanguage = value;
                    }
                }
            );
            angular.forEach(
                $scope.factions, function (value) {
                    if (value == SettingsService.get("initiative")) {
                        $scope.selectedInitiative = value;
                    }
                }
            );
        };

        $scope.exportSettings = function () {
            $scope.settingsData = base64.encode(JSON.stringify($scope.$storage));
            $scope.settingsSet = true;
            $scope.importData = false;

        };

        $scope.importSettings = function () {
            $scope.settingsData = null;
            $scope.settingsSet = true;
            $scope.importData = true;
        };

        $scope.import = function () {
            try {
                var data = JSON.parse(base64.decode($scope.settingsData));
                if (angular.isObject(data)) {
                    $scope.$storage.settings = data.settings;
                    $scope.$storage.ships = data.ships;
                    $scope.settingsData = null;
                    $scope.settingsSet = false;
                    $scope.importData = false;
                    Notification.success($translate.instant('success.import'));
                }
            } catch (e) {
                Notification.error($translate.instant('errors.import'));
            }
        };

        $scope.init();
    }]
);
