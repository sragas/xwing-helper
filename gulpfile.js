var gulp = require('gulp');
var util = require('gulp-util');
var clean = require('gulp-clean');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var minify = require('gulp-minify');
var jade = require('gulp-jade');
var concat = require('gulp-concat');
var rename = require("gulp-rename");
var manifest = require('gulp-manifest');
var electron = require('gulp-electron');
var merge = require('merge-stream');
var remoteSrc = require('gulp-remote-src');
var coffee = require('gulp-coffee');
var replace = require('gulp-replace');
var data = require('gulp-data');

// Compents path
var npmModules = "./node_modules/";
var bowerComponents = "./bower_components/";

// Src Path
var srcPath = "./src/";
var jsSrcPath = srcPath + "js/";
var scssSrcPath = srcPath + "sass/";
var jadeSrcPath = srcPath + "jade/";
var localeSrcPath = srcPath + "locale/";
var imgSrcPath = srcPath + "img/";
var electronSrcPath = srcPath + "electron/";

// Deploy Path
var deployPath = "./app/";
var cssDeployPath = deployPath + "css/";
var jsDeployPath = deployPath + "js/";
var localeDeployPath = deployPath + "locale/";
var htmlDeployPath = deployPath;
var imgDeployPath = deployPath + "img/";
var electronDeployPath = "./electron/";

var production = !!util.env.production;

var fileListJS = [
    npmModules + "angular/angular.min.js",
    npmModules + "jquery/dist/jquery.min.js",
    npmModules + "angular-route/angular-route.min.js",
    npmModules + "angular-translate/dist/angular-translate.min.js",
    npmModules + "angular-translate-loader-static-files/angular-translate-loader-static-files.min.js",
    npmModules + "ngstorage/ngStorage.min.js",
    npmModules + "angular-ui-bootstrap/dist/ui-bootstrap.js",
    npmModules + "angular-ui-bootstrap/dist/ui-bootstrap-tpls.js",
    npmModules + "angular-ui-notification/dist/angular-ui-notification.min.js",
    npmModules + "angular-utf8-base64/angular-utf8-base64.js",
    npmModules + "bootstrap-sass/assets/javascripts/bootstrap.min.js",
    npmModules + "ng-fittext/dist/ng-FitText.min.js"
];

var fileListFonts = [
    bowerComponents + "xwing-miniatures-font/dist/*.ttf",
    npmModules + "bootstrap-sass/assets/fonts/bootstrap/*.*"
];

var fileListSass = [
    bowerComponents + "xwing-miniatures-font/sass/*.sass"
];


gulp.task('build', ['build:css', 'build:html', 'build:js', 'build:locale', 'build:fonts', 'build:img', 'build:manifest']);

gulp.task('build:css', function () {
    var scssStream = gulp.src(scssSrcPath + "app.scss")
        .pipe(sass().on('error', sass.logError));

    var sassStream = gulp.src(fileListSass)
        .pipe(sass({indentedSyntax: true}).on('error', sass.logError));

    var mergedStream = merge(sassStream, scssStream)
        .pipe(concat('app.css'))
        .pipe(production ? minifyCss() : util.noop())
        .pipe(gulp.dest(cssDeployPath));

    return mergedStream;
});


gulp.task('build:js', ['copy:js-dep', 'copy:js-src']);

gulp.task('build:locale', ['copy:locale']);

gulp.task('build:fonts', ['copy:font-dep']);

gulp.task('clean', function () {
    return gulp.src(deployPath + '*')
        .pipe(clean({force: true, read: false}));
});

gulp.task('build:img', function () {
    return gulp.src(imgSrcPath + "**")
        .pipe(gulp.dest(imgDeployPath));
});

gulp.task('copy:js-dep', function () {
    return gulp.src(fileListJS)
        .pipe(concat('libraries.js'))
        .pipe(production ? minify({
            noSource: true,
            ext: {
                min: '.js'
            }
        }) : util.noop())
        .pipe(gulp.dest(jsDeployPath));
});

gulp.task('copy:js-src', function () {
    return gulp.src(jsSrcPath + "*.js")
        .pipe(production ? minify({
            noSource: true,
            ext: {
                min: '.js'
            }
        }) : util.noop())
        .pipe(gulp.dest(jsDeployPath));
});

gulp.task('copy:font-dep', function () {
    return gulp.src(fileListFonts)
        .pipe(gulp.dest(cssDeployPath));
});


gulp.task('copy:locale', ['copy:locale-dep'], function () {
    return gulp.src(localeSrcPath + "*.json")
        .pipe(gulp.dest(localeDeployPath));
});

gulp.task('copy:locale-dep', function () {
    return gulp.src(npmModules + "xwing-data/data/pilots.js")
        .pipe(rename('locale-en-pilots.json'))
        .pipe(gulp.dest(localeSrcPath));
});

gulp.task('build:html', function () {
    return gulp.src(jadeSrcPath + "/**/*.jade")
        .pipe(jade())
        .pipe(gulp.dest(htmlDeployPath))
});

gulp.task('build:manifest', ['build:css', 'build:html', 'build:js', 'build:locale', 'build:fonts', 'build:img'], function () {
    return gulp.src(deployPath + '**')
        .pipe(manifest({
            base: './',
            exclude: 'app.manifest',
            hash: true
        }))
        .pipe(gulp.dest(deployPath));
});

gulp.task('copy:app-src', ['clean:electron','build'], function () {
    return gulp.src(deployPath + '**')
        .pipe(gulp.dest(electronDeployPath + "app/"))
});

gulp.task('clean:electron', function () {
    return gulp.src(electronDeployPath + '*')
        .pipe(clean({force: true, read: false}));
});

gulp.task('build:electron', ['copy:app-src'], function () {
    return gulp.src(electronSrcPath + '**')
        .pipe(gulp.dest(electronDeployPath))
});

gulp.task('electron', ['build:electron'], function () {
    var packageJson = require(electronDeployPath + 'package.json');
    gulp.src("")
        .pipe(electron({
            src: electronDeployPath,
            packageJson: packageJson,
            release: './electron-release',
            cache: './electron-cache',
            version: 'v1.6.7',
            packaging: true,
            platforms: ['win32-ia32', 'darwin-x64'],
            platformResources: {
                darwin: {
                    CFBundleDisplayName: packageJson.name,
                    CFBundleIdentifier: packageJson.name,
                    CFBundleName: packageJson.name,
                    CFBundleVersion: packageJson.version,
                    icon: electronDeployPath + 'electron.icns'
                },
                win: {
                    "version-string": packageJson.version,
                    "file-version": packageJson.version,
                    "product-version": packageJson.version,
                    "icon": electronDeployPath + 'electron.ico'
                }
            }
        }))
        .pipe(gulp.dest(""));
});

gulp.task('update-pilot-cards-de', function () {
    var url = "https://raw.githubusercontent.com/geordanr/xwing/master/coffeescripts/";
    remoteSrc(['cards-de.coffee'], {
        base: url
    })
        .pipe(replace(/((.|\n)*?)(pilot_translations\s=)/m, "pilot_translations = "))
        .pipe(replace(/(upgrade_translations\s=)([\n\r]|.*)*/g, ""))
        .pipe(coffee({bare: true}).on('error', util.log))
        .pipe(data(function (file) {
            var content = eval(String(file.contents));
            content = JSON.stringify(content);
            file.contents = new Buffer(content);
        }))
        .pipe(rename('locale-de-pilots.json'))
        .pipe(gulp.dest(localeSrcPath));
});

gulp.task('default', ['clean'], function () {
    gulp.start('build');
});
