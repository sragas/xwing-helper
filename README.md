# X-Wing Miniatures  Round Helper
Just wanted to play around a bit with Angular JS and browser storage and as a result created a simple tool to help you and your friends to always get the right turn order of your pilots (especially helpful during a big epic game) for the [X-Wing Miniatures  Game](http://www.fantasyflightgames.com/edge_minisite.asp?eidm=174&enmi=X-Wing) by [Fantasy Flight Games](https://www.fantasyflightgames.com)

The tool is based on pure HTML, CSS and JavaScript ([AngularJS](https://angularjs.org/), [Bootstrap](http://getbootstrap.com/)) and uses local browser storage for data holding.
## Features
* Available in German and English
* Clear visualisation of active pilots
* Manage pilots by localized pilot name
* Edit skill values
* Place skill changes for next round on hold
* Delete destroyed ships
* Works completely in your modern browser (also offline)
* Fully responsive (best on large screens or tablet)
* Export and import your settings and pilot list

## Usage
Simplest usage is to visit https://sragas.gitlab.io/xwing-helper/ and start directly using the tool.

If you use a tablet, the tool also supports the **save to homescreen** feature, just ensure to run the saved app once with an active internet connection.


Included in the repository is also a standalone executable for OS X and Windows using the [Electron Framework](http://electron.atom.io/).

[Direct download OS X executable](https://sragas.gitlab.io/xwing-helper/xwing-helper-0.1.0-darwin-x64.zip)

[Direct download Windows executable](https://sragas.gitlab.io/xwing-helper/xwing-helper-0.1.0-win32-ia32.zip)


You could also download a fully runnable version as [zip file](https://sragas.gitlab.io/xwing-helper/app.zip) This could be placed on any webspace or webserver.

The repository also contains a small node.js server, just install the needed packages and start the server.  After extracting the above zip file or building the app.

`npm install --only=production`

`npm start`

The server will start on port **8080** on your machine. If you want to change the port just edit the *server.js* in the document root before starting.

## Building
1. Install [Node.js](http://nodejs.org/) to get `npm`.
2. `sudo npm install -g gulp` to install gulp globally.
3. `npm install` to install the dependencies from `package.json`.
4. `gulp` to build the app into the `app` directory.

## Credits
[Hinny](https://github.com/Hinny) and [Josh Derksen](https://github.com/armoredgear7) for their awesome [X-Wing Symbol and Ship Font](https://github.com/armoredgear7/xwing-miniatures-font).

[Guido Kessels](https://github.com/guidokessels/) for his more than practical [X-Wing dataset](https://github.com/guidokessels/xwing-data).

[Geordan Rosario](https://github.com/geordanr) for his cool [X-Wing Miniatures Squad Builder](http://geordanr.github.io/xwing/) which also includes german pilot translations.

## License
[MIT](https://sragas.gitlab.io/xwing-helper/LICENSE)

---
This tool is unofficial and is not affiliated with Fantasy Flight Games, Lucasfilm Ltd., or Disney.
Star Wars, X-Wing: The Miniatures Game and all related properties, images and text are owned by Fantasy Flight Games, Lucasfilm Ltd., and/or Disney.
